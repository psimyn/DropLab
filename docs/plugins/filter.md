# filter

filter is a droplab plugin that allows for filtering data that has been added
to the dropdown using a simple fuzzy string search of an input value.

## Install

* Add [`/dist/plugins/filter.js`](/dist/plugins/filter.js) to your application assets.
* Link to them in your code. See [examples](README.md#examples).

## Usage

The plugin listens to a trigger element's `keyup` events and will render a filtered list.

The plugin needs to know which data to filter and which value to perform a fuzzy search against.
Therefore, you must configure droplab using `DropLab.prototype.setConfig`.
The configuration object should have a key named after the `data-id` of the trigger element of the dropdown you want to use the plugin for. The value of this key should be an object with a value for key `text`, which is the key of the value from the data array items that you want to filter by.

```html
<input type="text" data-dropdown-trigger="#filter-dropdown" data-id="inputor">Toggle</a>

<ul id="filter-dropdown" data-dropdown>
  <li><a href="#" data-id="{{id}}">{{name}}</a></li>
</ul>
```

```js
droplab.setConfig({
  inputor: {
    text: 'name'
  }
});

droplab.addData('inputor', [{
  id: 0,
  name: 'Jacob'
}, {
  id: 1,
  name: 'Jeff'
}]);
```
